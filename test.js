const Operations = require('./index');

const noOfBlocks = 10;
const operation = new Operations(noOfBlocks);

// fetch latest blocks
operation.fetchLatestBlocks();


// address to query
const address = "0x84dd11Eb2A29615303D18149C0DBfA24167F8966";
operation.retrieveTransaction(address).then(data=>{
    console.log(data);
})


