var levelup = require('levelup')
var leveldown = require('leveldown')

class DataBase {
    constructor() {
        this.db = levelup(leveldown('./mydb'))
    }

    readFromDB(id) {
        return new Promise((resolve, reject) => {
            this.db.get(id, function (err, value) {
                if (err) {
                    reject(err)
                }
                resolve(value);
            })
        })
    }

    writeToDB(id, value){
        return new Promise((resolve, reject) =>{
            this.db.put(id, value, function(err){
                if(err){
                    reject(err)
                }
                resolve("Successfully wrote to db");
            })
        })
    }
}

module.exports = DataBase;