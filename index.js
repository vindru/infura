const Web3 = require('web3');
const db = require('./leveldb');

class Operations {
    constructor(noOfBlocks) {
        this.web3 = new Web3(new Web3.providers.HttpProvider("https://kovan.infura.io/v3/6c6f87a10e12438f8fbb7fc7c762b37c"));
        this.db = new db();
        this.noOfBlocks = noOfBlocks;
    }

    async fetchLatestBlocks() {
        const latest = await this.web3.eth.getBlockNumber();
        let addressToTransaction = {};

        for (let blockNo = 0; blockNo < this.noOfBlocks; blockNo++) {
            console.log("Fetching Block No: ", blockNo+1);
            let blockData = await this.web3.eth.getBlock(latest - blockNo);
            for (let transactions = 0; transactions < blockData.transactions.length; transactions++) {
                let trans = await this.web3.eth.getTransaction(blockData.transactions[transactions])
                if (addressToTransaction[trans.from] === undefined) {
                    addressToTransaction[trans.from] = [];
                }
                addressToTransaction[trans.from].push(trans.hash);

                if (trans.to === trans.from || trans.to === null) {
                    continue;
                }

                if (addressToTransaction[trans.to] === undefined) {
                    addressToTransaction[trans.to] = [];
                }
                addressToTransaction[trans.to].push(trans.hash);
            }
        }

        for (let key in addressToTransaction) {
            let dataToWrite = JSON.stringify(addressToTransaction[key]);
            await this.db.writeToDB(key, dataToWrite);
        }

    }

    async retrieveTransaction(address) {
        let transArray = [];
        return new Promise((resolve, reject)=>{

            this.db.readFromDB(address).then(async data => {
                let listOfTransactions = JSON.parse(data.toString());
                let transCount = listOfTransactions.length;
                let transactionDetails;
    
                for (let trans = 0; trans < transCount; trans++) {
                    transactionDetails = await this.web3.eth.getTransaction(listOfTransactions[trans]);
                    let pushData = {
                        to: transactionDetails.to,
                        from: transactionDetails.from,
                        blockNumber: transactionDetails.blockNumber,
                        blockHash: transactionDetails.blockHash
                    }
                    transArray.push(pushData);
                }
                resolve(transArray);
            })
        }).catch(err=>{
            reject(err);
        })

    }

}

module.exports = Operations;